module.exports = {
  productionSourceMap: false,
  // publicPath: '/VUE_APP_API_BASE_URL', // dev
  lintOnSave: process.env.NODE_ENV !== "production",
}