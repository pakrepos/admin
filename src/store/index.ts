import { createStore, Module } from "vuex";

import user from "./modules/user";
import tree from "./modules/tree";
import datatable from "./modules/datatable";
import modal from "./modules/modal";
import dictionary from "./modules/dictionary";

export interface RootState {
  // GlobalState
}

export default createStore<RootState>({
  modules: {
    user,
    tree,
    datatable,
    modal,
    dictionary
  },
  state: {},
  mutations: {},
  actions: {}
});
