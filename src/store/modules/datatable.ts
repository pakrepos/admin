import { Module } from "vuex";
import { RootState } from "..";
import axios from "axios";

import usePromise from "@/components/hooks/use-promise";
import api, { createDictionaryApi } from "@/services/api";

export interface State {
  selectedRowKeys: any[];
  data: any;
  refreshTableWatcher: boolean;
}

const datatable: Module<State, RootState> = {
  state: {
    selectedRowKeys: [],
    data: [],
    refreshTableWatcher: false
  },

  mutations: {
    setSelectedRowKeys(state, payload) {
      state.selectedRowKeys = payload;
    },
    setData(state, payload) {
      enum ResUrls {
        product = "products",
        ingredients = "ingredients",
        tags = "tags",
        parameter = "parameters"
      }
      const { createPromise, ...statePromise } = usePromise(
        createDictionaryApi(payload.block?.url, payload.block?.readonly)
      );

      function itemAction(action = "getData") {
        return createPromise({ action });
      }

      if (["getData"].includes(payload.action)) {
        createPromise(
          { action: payload.action, data: payload.value?.data.value },
          { noresult: true }
        )
          .then(() => {
            return itemAction();
          })
          .then(res => {
            state.data = res[ResUrls[payload.block?.url]];
          });
      }
      if (["addData"].includes(payload.action)) {
        createPromise(
          { action: payload.action, data: payload.value?.data.value },
          { noresult: true }
        )
          .then(() => {
            return itemAction();
          })
          .then(res => {
            state.data = res[ResUrls[payload.block?.url]];
            state.selectedRowKeys = [];
          });
      }
      if (["addProductParameter"].includes(payload.action)) {
        delete payload.value.data.value["is_active"];
        payload.value.data.value["product_id"] = this.state.modal.rowId;

        axios({
          url: `https://yummepal-backend.herokuapp.com/api/parameter/create`,
          method: "POST",
          data: payload.value.data.value
        })
          .then(() => {
            return itemAction();
          })
          .then(res => {
            state.data = res[ResUrls[payload.block?.url]];
            state.selectedRowKeys = [];
          });
      }
      if (["addSubData"].includes(payload.action)) {
        function doAxios(addToUrl, row) {
          if (addToUrl === "tags") {
            axios({
              url: `https://yummepal-backend.herokuapp.com/api/${addToUrl}/productAttach/${row}`,
              method: "POST",
              data: {
                tags:
                  payload.value.data.value[
                    Object.keys(payload.value.data.value)[0]
                  ]
              }
            })
              .then(() => {
                return itemAction();
              })
              .then(res => {
                state.data = res[ResUrls[payload.block?.url]];
                state.selectedRowKeys = [];
              });
          }
          if (addToUrl === "ingredients") {
            axios({
              url: `https://yummepal-backend.herokuapp.com/api/${addToUrl}/productAttach/${row}`,
              method: "POST",
              data: {
                ingredients:
                  payload.value.data.value[
                    Object.keys(payload.value.data.value)[0]
                  ]
              }
            })
              .then(() => {
                return itemAction();
              })
              .then(res => {
                state.data = res[ResUrls[payload.block?.url]];
                state.selectedRowKeys = [];
              });
          }
        }
        let addToUrl = null;
        if (payload.subData === "ingridientsList") {
          addToUrl = "ingredients";
          const row = this.state.modal.rowId;
          doAxios(addToUrl, row);
        }
        if (payload.subData === "tagList") {
          addToUrl = "tags";
          const row = this.state.modal.rowId;
          doAxios(addToUrl, row);
        }
      }
      if (["removeData"].includes(payload.action)) {
        createPromise(
          { action: payload.action, data: payload.value?.data },
          { noresult: true }
        )
          .then(() => {
            return itemAction();
          })
          .then(res => {
            state.data = res[ResUrls[payload.block?.url]];
            state.selectedRowKeys = [];
          });
      }
      if (["saveData"].includes(payload.action)) {
        createPromise(
          {
            action: payload.action,
            data: payload.value?.data.value,
            id: payload.value?.data.value.id
          },
          { noresult: true }
        )
          .then(() => {
            return itemAction();
          })
          .then(res => {
            state.data = res[ResUrls[payload.block?.url]];
            state.selectedRowKeys = [];
          });
      }
    }
  },

  actions: {},

  getters: {
    getSelectedRowKeys(state) {
      return state.selectedRowKeys;
    },
    getPromiseItem(state) {
      return state.data;
    },
    getRefreshTable(state) {
      return state.refreshTableWatcher;
    }
  }
};

export default datatable;
