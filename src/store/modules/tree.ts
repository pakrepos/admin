import { Module } from "vuex";
import { RootState } from "..";
import { ArrayHelper } from "@/helpers/array";
import api from "@/api/auth";

import Navigation from "@/components/exampleResponseFromDB/navigation.json";
import moment from "moment";

export interface State {
  tree: any;
}

const treeStore: Module<State, RootState> = {
  state: {
    tree: [
      {
        id: 1,
        name: "Продукты",
        kind: null,
        navType: "products",
        child: [
          {
            id: 11,
            name: "Товары",
            kind: "ProductsList",
            navType: null,
            child: []
          },
          {
            id: 12,
            name: "Ингридиенты",
            kind: "IngridientsList",
            navType: null,
            child: []
          },
          {
            id: 13,
            name: "Тэги",
            kind: "TagList",
            navType: null,
            child: []
          }
        ]
      }
      // {
      //   id: 2,
      //   name: "Промокоды",
      //   kind: "Promocodes",
      //   navType: "products",
      //   child: []
      // }
    ]
  },

  mutations: {
    setTree(payload) {
      this.state.tree = payload;
    },
    setFlatTree(payload) {
      this.state.flatTree = payload;
    }
  },

  actions: {},

  getters: {
    getTree(state) {
      return state.tree;
    },
    getTreeId(state) {
      return id => state.tree.find(el => el.id == id) || { submenu: [] };
    },
    getModulesIds(state) {
      return state.tree.map(item => item.id) ?? [];
    }
  }
};

export default treeStore;
