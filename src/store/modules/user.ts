import { Module } from "vuex";
import { RootState } from "..";
import router from "../../router/index";

import axios from "axios";
import { useNoty } from "@/services/notification";

export interface State {
  token: string;
  isAuth: boolean;
  userLoading: boolean;
}

const user: Module<State, RootState> = {
  state: {
    token: "",
    isAuth: false,
    userLoading: false
  },

  mutations: {
    checkAuth(state, payload) {
      if (localStorage.getItem("PAKtokenAPI")) {
        state.isAuth = true;
      } else {
        state.isAuth = false;
      }
    },
    logout(state, payload) {
      state.userLoading = true;
      axios({
        url: `https://yummepal-backend.herokuapp.com/api/user/dropToken`,
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("PAKtokenAPI")}`
        }
      }).then(() => {
        localStorage.removeItem("PAKtokenAPI");
        state.isAuth = false;
        router.push({ path: "/login" });
        state.userLoading = false;
      });
    },
    // test@gmail.com          adminSuperUser
    // admin@yumme.ru          admin
    setToken(state, payload?) {
      state.userLoading = true;
      axios({
        url: `https://yummepal-backend.herokuapp.com/api/getToken`,
        method: "POST",
        data: { email: payload.email, password: payload.password }
      })
        .then(res => {
          if (res.status === 200) {
            axios({
              url: `https://yummepal-backend.herokuapp.com/api/user/me`,
              method: "GET",
              headers: {
                Authorization: `Bearer ${res.data["token"]}`
              }
            }).then(response => {
              if (response.status === 200) {
                if (response.data.user.role === 1) {
                  localStorage.setItem("PAKtokenAPI", res.data["token"]);
                  state.token = res.data["token"];
                  router.push({ path: "/" });
                  useNoty({
                    type: "success",
                    message: "Вход выполнен успешно"
                  }).openNotification();
                  state.isAuth = true;
                  state.userLoading = false;
                } else {
                  useNoty({
                    type: "error",
                    message: "Нет прав доступа"
                  }).openNotification();
                  state.userLoading = false;
                }
              }
              if (response.status === 404) {
                useNoty({
                  type: "error",
                  message: "Такого пользователя не существует"
                }).openNotification();
                state.userLoading = false;
              }
            });
          }
          if (res.status === 404) {
            useNoty({
              type: "error",
              message: "Такого пользователя не существует"
            }).openNotification();
            state.userLoading = false;
          }
        })
        .catch(res => {
          useNoty({
            type: "error",
            message: "Такого пользователя не существует"
          }).openNotification();
          state.userLoading = false;
        });
    }
  },

  actions: {},

  getters: {
    getUserToken(state) {
      return localStorage.getItem("PAKtokenAPI");
    },
    getAuth(state) {
      return state.isAuth;
    },
    getUserLoading(state) {
      return state.userLoading;
    }
  }
};

export default user;
