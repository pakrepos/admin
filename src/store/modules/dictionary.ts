import { Module } from "vuex";
import { RootState } from "..";
import moment from "moment";
import { config } from "@/components/blocks";

export interface State {
  globalFilter: any;
  dictionaries: any;
  modalDictionaries: any;
  treeDictionary: any[];
  config: any;
}

const dictionary: Module<State, RootState> = {
  state: {
    globalFilter: {
      actualDate: moment()
        .utcOffset(0, true)
        .startOf("day")
        .format()
    },
    dictionaries: {},
    modalDictionaries: {},
    treeDictionary: [],
    config: config
  },

  mutations: {
    changeGlobalFilter(state, { filter, value }) {
      if (
        Object.prototype.hasOwnProperty.call(this.state.globalFilter, filter)
      ) {
        state.globalFilter[filter] = value;
      }
    },
    setDictionaries(state, payload) {
      state.dictionaries = payload;
    },
    setModalDictionaries(state, payload) {
      state.modalDictionaries = payload;
    },
    setTreeDictionaries(state, payload) {
      state.treeDictionary = payload;
    }
  },

  actions: {},

  getters: {}
};

export default dictionary;
