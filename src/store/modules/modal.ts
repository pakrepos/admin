import { Module } from "vuex";
import { RootState } from "..";

export interface State {
  modal: any;
  subAddColumn: any;
  rowId: number;
  tagParams: any;
}

const modal: Module<State, RootState> = {
  state: {
    modal: {
      showAdd: false,
      showSubAdd: false,
      showModalAddProductParameter: false,
      showEdit: false,
      showEditProductParameter: false,
      showDelete: false,
      showActiveEdit: false
    },
    subAddColumn: null,
    rowId: 0,
    tagParams: null
  },

  mutations: {
    toggleModal(state, params: { typeShow: string; value: boolean }) {
      if (Object.keys(state.modal).includes(params.typeShow)) {
        for (const item of Object.keys(state.modal)) {
          state.modal[item] = item === params.typeShow && params.value;
        }
      }
    },
    setRowId(state, params: { rowId: number }) {
      state.rowId = params.rowId;
    },
    setSubAddColumn(state, params: { subAddColumn: any }) {
      state.subAddColumn = params.subAddColumn;
    },
    setParams(state, params: { tagParams: any }) {
      state.tagParams = params.tagParams;
    }
  },

  actions: {},

  getters: {
    getModalVisibility(state) {
      return state.modal;
    },
    getRowId(state) {
      return state.rowId;
    },
    getSubAddColumn(state) {
      return state.subAddColumn;
    },
    getTagParams(state) {
      return state.tagParams;
    }
  }
};

export default modal;
