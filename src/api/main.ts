import axios from "axios";

const AUTH_URL = config.authUrl;

export default {
  init() {
    axios.defaults.baseURL = "https://api.example.com";
  },

  login(credension: FormData) {
    return axios.post("login/domain", credension, { baseURL: AUTH_URL });
  },
  refresh(refreshToken: string) {
    return axios.post("login/refresh", null, {
      params: { refreshToken },
      baseURL: AUTH_URL,
    });
  },
};
