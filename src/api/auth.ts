import axios, { AxiosRequestConfig } from "axios";
import { useStore } from "vuex";

const store = useStore();
const API_URL = config.apiUrl;

function defaultConfig(): AxiosRequestConfig {
  return {
    baseURL: API_URL,
    headers: {
      Authorization: store.getters["tokenAuth"](),
    },
  };
}

interface IDataParam {
  actualDate?: string;
}

const createAxios = <T>(axiosConfig: AxiosRequestConfig) =>
  axios.request<T>({ ...axiosConfig, ...defaultConfig() });

const api = {
  reference: {
    administration: {
      treeMenu: {
        getRoots: (params: IDataParam) =>
          createAxios({
            method: "GET",
            url: "/reference/administration/tree-menu/roots",
            params,
          }),
      },
    },
  },
};

export default api;
