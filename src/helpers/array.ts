interface IFlatObject {
  [k: string]: string;
}

interface IObject {
  [k: string]: string | IObject[];
}

export class ArrayHelper {
  constructor(private array: Array<IObject>) {}

  public flat(fields: string[] = []): IFlatObject[] {
    if (!Array.isArray(fields) || fields.length === 0) {
      return [];
    }
    return this.recursiveFlat(fields, this.array);
  }

  private recursiveFlat(fields: string[], items: IObject[]): IFlatObject[] {
    const localItems: IFlatObject[] = [];
    for (const row of items) {
      let isParent = false;
      fields.forEach(field => {
        if (Object.prototype.hasOwnProperty.call(row, field)) {
          const item = row[field];
          if (Array.isArray(item) && item.length > 0) {
            isParent = true;
            localItems.push(...this.recursiveFlat(fields, item));
          }
        }
      });
      if (!isParent) {
        localItems.push(row as IFlatObject);
      }
    }
    return localItems;
  }
}
