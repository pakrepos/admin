import { computed, ComputedRef, inject, reactive, ref, watch } from "vue";
import { useStore } from "vuex";
import axios from "axios";

import { ConfigBlock, IConfigColumn } from "../blocks";

export interface IDataRow {
  id: number;
  name: string;
  version: number;
  isActive: boolean;
  isActiveOnFuture: boolean;
  existOnDate: boolean;
  toDate: string;
  fromDate: string;
  value?: any;
  [k: string]: any;
}

export default function useDatatable(props) {
  const { value: block } = inject<ComputedRef<ConfigBlock>>("block");
  const store = useStore();
  const columns = reactive<IConfigColumn[]>([
    {
      key: "id",
      keyTable: "id",
      title: "ID",
      type: "readonly",
      width: 80
    },
    ...(block?.beforeColumn ?? []),
    ...(block?.column ?? [
      {
        key: "name",
        keyTable: "name",
        title: "Наименование",
        icon: "FontSize",
        type: "input",
        width: 200
      }
    ]),
    ...(block?.middleColumn ?? []),
    ...(block?.afterColumn ?? []),
    ...(block ? [{ type: "action", key: "id", keyTable: "id", width: 80 }] : [])
  ]);

  const filters = reactive(
    columns
      .map(col => col.key)
      .reduce((acc, curr) => ((acc[curr] = undefined), acc), {})
  );
  watch(filters, value => {
    // broadcast.sendTableMessage({ action: 'changeFilter', value: { ...value, from: 0 } }); ПЕРЕДЕЛАТЬ
    currentPage.value = 1;
  });

  const currentPage = ref(1);

  const tableData = computed<IDataRow>(() => props.value);

  const pagination = computed(() => ({
    pageSizeOptions: ["10", "15", "20"]
  }));

  const dataSource = ref();

  const _selectedRowKeys = ref([]);
  const _selectedRow = ref([]);
  const rowSelection = computed(() => ({
    selectedRowKeys: store.getters.getSelectedRowKeys,
    onChange: (selectedRowKeys, selectedRows) => {
      _selectedRowKeys.value = selectedRowKeys;
      _selectedRow.value = selectedRows;
      store.commit("setSelectedRowKeys", _selectedRowKeys.value);
    },
    getCheckboxProps: record => ({
      props: {
        name: record.name
      }
    })
  }));
  const showEdit = function(record) {
    if (!block?.readonly) {
      store.commit("setRowId", { rowId: record });
      store.commit("toggleModal", { typeShow: "showEdit", value: true });
    }
  };
  const showModalEditProductParameter = function(record) {
    if (!block?.readonly) {
      store.commit("setRowId", { rowId: record });
      store.commit("toggleModal", {
        typeShow: "showEditProductParameter",
        value: true
      });
      store.commit("setParams", { tagParams: record.tag });
    }
  };
  const showSubAdd = function(record) {
    if (!block?.readonly) {
      store.commit("setRowId", { rowId: record.id });
      store.commit("setSubAddColumn", { subAddColumn: record.column });
      store.commit("toggleModal", { typeShow: "showSubAdd", value: true });
    }
  };
  const showModalAddProductParameter = function(record) {
    if (!block?.readonly) {
      store.commit("setRowId", { rowId: record.id });
      store.commit("setSubAddColumn", { subAddColumn: record.column });
      store.commit("toggleModal", {
        typeShow: "showModalAddProductParameter",
        value: true
      });
    }
  };
  const deleteTag = function(productIndex, tagIndex) {
    axios({
      url: `https://yummepal-backend.herokuapp.com/api/tags/productDetach/${productIndex}`,
      method: "POST",
      data: {
        tags: [tagIndex]
      }
    }).then(() => {
      store.commit("setData", { block: block, action: "getData" });
    });
  };
  const deleteIngredient = function(productIndex, ingredientIndex) {
    axios({
      url: `https://yummepal-backend.herokuapp.com/api/ingredients/productDetach/${productIndex}`,
      method: "POST",
      data: {
        ingredients: ingredientIndex
      }
    }).then(() => {
      store.commit("setData", { block: block, action: "getData" });
    });
  };
  const deleteParams = function(productIndex, paramIndex) {
    axios({
      url: `https://yummepal-backend.herokuapp.com/api/parameter/delete/${paramIndex}`,
      method: "DELETE"
    }).then(() => {
      store.commit("setData", { block: block, action: "getData" });
    });
  };
  const customTableRow = function(record) {
    return {
      onClick: event => {
        if (!event.target.className.includes("ant-btn")) {
          const index = _selectedRowKeys.value.indexOf(record.id);
          if (index > -1) {
            _selectedRowKeys.value.splice(index, 1);
            _selectedRow.value.splice(
              _selectedRow.value.findIndex(row => row.id === record.id),
              1
            );
            store.commit("setSelectedRowKeys", _selectedRowKeys.value);
          } else {
            _selectedRowKeys.value.push(record.id);
            _selectedRow.value.push(record.id);
            store.commit("setSelectedRowKeys", _selectedRowKeys.value);
          }
        }
      },
      onDblclick: () => {
        showEdit(record.id);
      }
    };
  };

  const tableDinamicalAttribute = function() {
    return {
      rowSelection: rowSelection.value,
      pagination: dataSource.value,
      customRow: customTableRow
    };
  };

  return {
    block,
    columns,
    filters,
    tableData,
    // pagination,
    showEdit,
    showSubAdd,
    customTableRow,
    rowSelection,
    tableDinamicalAttribute,
    deleteTag,
    deleteIngredient,
    showModalAddProductParameter,
    showModalEditProductParameter,
    deleteParams
  };
}
