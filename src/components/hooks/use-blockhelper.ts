import { computed, ComputedRef, inject, ref } from "vue";
import { ITreeItem } from "../blocks";
import { useStore } from "vuex";

interface IParseAdditionalAttrsParam {
  kind?: string;
}

export default function useBlockHelper() {
  const store = useStore();

  const parsedValue = ref({});
  const moduleId = inject<ComputedRef<string>>("moduleId");
  const pageId = inject<ComputedRef<string>>("parseId");

  const parsedAdditionalAttrs = function(
    params: IParseAdditionalAttrsParam = {},
  ): any {
    if (params.kind) {
      const modulesId = computed<Array<number>>(() =>
        store.getters["getModuleIds"](),
      );
      const findItem = computed<{ item: ITreeItem; moduleId: number }>(() => {
        let findItem: ITreeItem;
        let moduleId: number;
        modulesId.value.forEach(_moduleId => {
          const item = store.getters["getFlatTreeId"]()(_moduleId)?.child?.find(
            treeItem => treeItem.kind === params.kind,
          );
          if (item) {
            findItem = item;
            moduleId = _moduleId;
            return;
          }
        });
        return { item: findItem, moduleId };
      });
      return {
        treeBlock: computed<ITreeItem>(() => findItem.value.item),
        moduleId: computed<number>(() => findItem.value.moduleId),
      };
    }
    const flatTreeId = computed(() => store.getters["getFlatTreeId"]());
    return {
      treeBlock: computed<ITreeItem>(() =>
        flatTreeId
          .value(moduleId.value)
          .child.find(treeItem => treeItem.id === +pageId.value),
      ),
    };
  };
  return { parsedValue, parsedAdditionalAttrs };
}
