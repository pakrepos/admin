import { ref, Ref } from "vue";
import { AxiosError } from "axios";
import { useNoty } from "@/services/notification";

export default function usePromise(fn) {
  const loading = ref(false);
  const result: Ref<any> = ref(null);
  const error = ref(null);

  const createPromise = async (
    args = undefined,
    { noresult = false, onSuccess = () => {} } = {},
  ) =>
    new Promise(async (resolve, reject) => {
      loading.value = true;
      error.value = null;
      if (!noresult) {
        result.value = null;
      }
      fn(args)
        .then(_result => {
          if (!noresult) {
            result.value = _result?.data;
          }
          resolve(_result?.data);
          loading.value = false;
        })
        .catch(err => {
          const ex: AxiosError = err;
          const { data, status, headers } = ex.response || {};
          const typeResponce: string = headers["content-type"];
          if (status === 500) {
            if (Object.prototype.hasOwnProperty.call(data, ["Message"])) {
              useNoty({
                type: "error",
                message: data.Message,
              }).openNotification();
            } else {
              useNoty({ type: "error", message: data }).openNotification();
            }
          }
          if (status === 400 && typeResponce.includes("text/plain")) {
            useNoty({ type: "error", message: data }).openNotification();
          }
          if (status === 400 && typeResponce.includes("application/json")) {
            if (Object.prototype.hasOwnProperty.call(data, ["Validations"])) {
              useNoty({ type: "error", message: data }).openNotification();
            } else {
              useNoty({
                type: "error",
                message: "not-found-errors-key",
              }).openNotification();
            }
          }
          error.value = ex;
          reject(ex);
          loading.value = false;
        });
    });

  return {
    loading,
    error,
    result,
    createPromise,
  };
}
