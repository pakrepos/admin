import {
  computed,
  Ref,
  ComputedRef,
  inject,
  onMounted,
  ref,
  watch,
  createVNode
} from "vue";
import { Modal } from "ant-design-vue";

import { ExclamationCircleOutlined } from "@ant-design/icons-vue";

import { ConfigBlock, IConfigColumn } from "@/components/blocks";
import { useStore } from "vuex";
import { useNoty } from "@/services/notification";

export function useCRUD(props: {
  onModalSuccess: string;
  action: string;
  showModal: string;
  successText?: string;
}) {
  const store = useStore();
  const { value: block } = inject<ComputedRef<ConfigBlock>>("block");

  const form = ref(null);
  const blockColumn: IConfigColumn[] = [
    ...(block?.beforeColumn ?? []),
    ...(block?.column ?? [
      {
        key: "name",
        title: "Наименование",
        type: "input",
        icon: "font-size",
        validator: [
          { required: true, message: "Введите наименование", trigger: "blur" }
        ],
        onBlur: () => {}
      }
    ]),
    ...(block?.middleColumn ?? []),
    ...(block?.afterColumn ?? [])
  ];

  const columnsParameters = [
    {
      key: "size",
      keyTable: "size",
      title: "Размер",
      type: "select",
      isArray: false,
      validator: [
        { required: true, message: "Введите размер", trigger: "blur" }
      ],
      options: [
        {
          label: "25",
          value: 25
        },
        {
          label: "30",
          value: 30
        },
        {
          label: "35",
          value: 35
        }
      ]
    },
    {
      key: "weight",
      keyTable: "weight",
      title: "Вес",
      type: "priceInput",
      isArray: false,
      validator: [{ required: true, message: "Введите вес", trigger: "blur" }],
      suffix: "г"
    },
    {
      key: "calories",
      keyTable: "calories",
      title: "Калории",
      type: "input",
      isArray: false,
      validator: [
        { required: true, message: "Введите калории", trigger: "blur" }
      ]
    },
    {
      key: "price",
      keyTable: "price",
      title: "Цена",
      type: "priceInput",
      isArray: false,
      validator: [{ required: true, message: "Введите цену", trigger: "blur" }],
      suffix: "₽"
    }
  ];

  const defaultValidator = [
    { trigger: "change", validator: (rule, value, cb) => cb() }
  ];
  const rules = {
    ...blockColumn.reduce(
      (prev, curr) => (
        (prev[curr.key] = curr.validator ?? defaultValidator), prev
      ),
      {}
    )
  };

  const formData = ref<{ [key: string]: any }>({
    ...blockColumn.reduce(
      (prev, { key, defaultValue }) => ((prev[key] = defaultValue), prev),
      {}
    )
  });

  const defaultFormData = ref({
    ...formData.value
  });

  const visible = ref(false);
  const _visible = ref(false);

  onMounted(() => {
    visible.value = true;
    _visible.value = true;
  });

  const changeDefaultFormData = function(data) {
    defaultFormData.value = { ...data };
  };

  const getChangesFormData = function(): Array<any> {
    return Object.entries(formData.value)
      .filter(
        ([field, value]) =>
          field !== "fromDate" &&
          defaultFormData.value[field] != value &&
          !(
            [undefined, null, ""].includes(defaultFormData.value[field]) &&
            [undefined, null, ""].includes(value)
          )
      )
      .map(([key, value]) => ({ [key]: value }));
  };

  const isDirtyForm = function(): boolean {
    return getChangesFormData().length > 0;
  };

  const sendData = function(data) {
    console.log(data);

    if (data.value?.name && props.action === "removeData") {
      if (data.value.parameters) {
        data.value.parameters.gift.discount_order =
          data.value.parameters.gift.discount_order / 100;
      }

      store.commit("setData", {
        block: block,
        action: props.action,
        value: { data, onModalSuccess: props.onModalSuccess }
      });
      if (props.action === "removeData") {
        useNoty({
          type: "success",
          message: props.successText
        }).openNotification();
      }
    } else {
      for (var propName in data.value) {
        if (data.value[propName] === undefined) {
          delete data.value[propName];
        }
      }
      store.commit("setData", {
        block: block,
        action: props.action,
        value: { data, onModalSuccess: props.onModalSuccess },
        isSubData: true,
        subData: data.value ? Object.keys(data.value)[0] : ""
      });
    }
    visible.value = false;
    _visible.value = false;
    store.commit("toggleModal", { typeShow: props.showModal, value: false });
    store.commit("setSelectedRowKeys", []);
  };

  const handleClose = function(force = false) {
    if (!force && isDirtyForm()) {
      Modal.confirm({
        title: () => "Вы уверены что хотите закрыть окно?",
        icon: () => createVNode(ExclamationCircleOutlined),
        content: () => "Внимание, внесенные изменения не сохранятся",
        onOk() {
          handleClose(true);
        },
        onCancel() {}
      });
      return;
    }
    visible.value = false;
    _visible.value = false;
    store.commit("toggleModal", { typeShow: props.showModal, value: false });
  };

  const handleOk = function() {
    sendData(formData);
    useNoty({
      type: "success",
      message: props.successText
    }).openNotification();
    // return form.value
    //   .validate()
    //   .then(() => {
    //     sendData(formData);
    //     useNoty({
    //       type: "success",
    //       message: props.successText
    //     }).openNotification();
    //   })
    //   .catch(e => {});
  };

  watch(visible, value => {
    if (value !== _visible.value) {
      visible.value = _visible.value;
    }
  });

  return {
    rules,
    form,
    blockColumn,
    visible,
    formData,
    handleClose,
    handleOk,
    changeDefaultFormData,
    isDirtyForm,
    getChangesFormData,
    sendData,
    columnsParameters
  };
}
