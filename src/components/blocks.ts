import ActionToolbar from "@/components/core/ActionToolbar.vue";
import Table from "@/components/dictionary/products/Item.vue";

import { ref, ComputedRef } from "vue";

export interface IConfig {
  [k: string]: ConfigBlock;
}

export interface ITreeItem {
  id: number;
  name: string;
  kind: string;
  child: ITreeItem[];
  [k: string]: any;
}

type TAvailableType = "readonly" | "noCreate" | "noDelete" | "roots";

const referenceKind = ref([]);

interface IConfigColumnOptionsItem {
  label?: string;
  value?: string;
}

export interface IConfigTemplateBlock {
  toolbar?: any;
  table?: any;
}

export interface IConfigBlock {
  title?: string;
  _title?: string;
  url: string;
  mode?: TAvailableType[];
  beforeColumn?: IConfigColumn[];
  column?: IConfigColumn[];
  middleColumn?: IConfigColumn[];
  afterColumn?: IConfigColumn[];
  template?: IConfigTemplateBlock;
  mixin?: {
    table?: Function;
    modalAdd?: Function;
    modalEdit?: Function;
  };
  dictionaries?: {
    [k: string]: any;
    preload?: string[];
    request?: string[];
    transformResult?: (value: any) => any;
  };
}

export interface IConfigColumn {
  keyTable?: string;
  title?: string;
  key?: string;
  type?: string;
  readonly?: boolean;
  hidden?: boolean;
  isHidden?: (row) => boolean;
  request?: string;
  part?: string;
  options?: IConfigColumnOptionsItem[];
  icon?: string;
  suffix?: string;
  dictionary?: string;
  number?: {
    min?: number;
  };
  precision?: number;
  formatter?: any;
  filter?: (item: any) => boolean;
  /**
   * @example validator: [ { required: true, message: 'Заполните наименование', trigger: 'blur' } ],
   */
  validator?: any[];
  isArray?: boolean;
  defaultValue?: any;
  onChange?: (value?: any) => void;
  onBlur?: () => void;
  noFilter?: boolean;
  noAllowClear?: boolean;
  noDisableDate?: boolean;
  width?: number;
}

export class ConfigBlock implements IConfigBlock {
  public template: IConfigTemplateBlock = {};
  public url = "";
  public mode?: TAvailableType[];
  public beforeColumn?: IConfigColumn[];
  public column?: IConfigColumn[];
  public middleColumn?: IConfigColumn[];
  public afterColumn?: IConfigColumn[];
  public dictionaries?: {
    preload: string[];
    transformResult?: (value: any) => any;
  };
  public mixin?: {
    table?: Function;
    modalAdd?: Function;
    modalEdit?: Function;
  };
  public _title?: string;
  private _treeBlock = ref({ name: "" });

  constructor(params: IConfigBlock) {
    Object.keys(params).forEach(key => {
      this[key] = params[key];
    });
  }

  public get readonly(): boolean {
    return this.mode?.includes("readonly");
  }

  public hasMode(value: TAvailableType): boolean {
    return this.mode?.includes(value);
  }

  public get templateBlocks(): IConfigTemplateBlock {
    return {
      toolbar: ActionToolbar,
      table: Table,
      ...this.template
    };
  }

  public setAdditionalAttrs(params): void {
    for (const [key, computed] of Object.entries<ComputedRef<any>>(params)) {
      this[`_${key}`].value = computed.value;
    }
  }
}

export const config: IConfig = {
  ProductsList: new ConfigBlock({
    title: "Товары",
    url: "product",
    middleColumn: [
      {
        key: "image",
        keyTable: "image",
        title: "Изображение",
        type: "upload",
        width: 130
      },
      {
        key: "ingridientsList",
        keyTable: "ingridientsList",
        title: "Ингридиенты",
        type: "dictionary",
        dictionary: "IngridientsList",
        isArray: true,
        hidden: true
      },
      {
        key: "tagList",
        keyTable: "tagList",
        title: "Тэги",
        type: "dictionary",
        dictionary: "TagList",
        isArray: true,
        hidden: true,
        width: 230
      },
      {
        key: "parameters",
        keyTable: "parameters",
        title: "Параметры",
        type: "dictionary",
        isArray: true,
        hidden: true,
        width: 200
      },
      {
        key: "is_active",
        keyTable: "is_active",
        title: "В наличии",
        icon: "check",
        type: "tableBoolean",
        defaultValue: true,
        options: [
          { label: "Да", value: "true" },
          { label: "Нет", value: "false" }
        ],
        width: 90
      }
    ],
    template: {
      table: Table,
      toolbar: ActionToolbar
    },
    dictionaries: {
      preload: ["IngridientsList", "TagList"]
    }
  }),
  IngridientsList: new ConfigBlock({
    title: "Ингридиенты",
    url: "ingredients",
    middleColumn: [
      {
        key: "price",
        keyTable: "price",
        title: "Цена",
        type: "priceInput",
        isArray: false,
        suffix: "₽",
        validator: [
          { required: true, message: "Введите цену", trigger: "blur" }
        ]
      },
      {
        key: "calories",
        keyTable: "calories",
        title: "Калории",
        type: "input",
        isArray: false,
        validator: [
          {
            required: true,
            message: "Введите количество калорий",
            trigger: "blur"
          }
        ]
      },
      {
        key: "count",
        keyTable: "count",
        title: "Количество",
        type: "input",
        isArray: false,
        validator: [
          { required: true, message: "Введите количество", trigger: "blur" }
        ]
      },
      {
        key: "weight",
        keyTable: "weight",
        title: "Вес",
        type: "priceInput",
        isArray: false,
        validator: [
          { required: true, message: "Введите вес", trigger: "blur" }
        ],
        suffix: "г"
      }
    ],
    template: {
      table: Table,
      toolbar: ActionToolbar
    }
  }),
  TagList: new ConfigBlock({
    title: "Тэги",
    url: "tags",
    middleColumn: [
      {
        key: "color",
        keyTable: "color",
        title: "Цвет",
        type: "colorpicker",
        isArray: false,
        validator: [
          { required: true, message: "Выберите цвет", trigger: "blur" }
        ]
      }
    ],
    template: {
      table: Table,
      toolbar: ActionToolbar
    }
  }),
  Parameters: new ConfigBlock({
    title: "Параметры",
    url: "parameter",
    middleColumn: [
      {
        key: "product_id",
        keyTable: "product_id",
        title: "Продукт",
        type: "dictionary",
        dictionary: "ProductsList",
        isArray: false,
        validator: [
          { required: true, message: "Выберите продукт", trigger: "blur" }
        ]
      },
      {
        key: "size",
        keyTable: "size",
        title: "Размер",
        type: "input",
        isArray: false,
        validator: [
          { required: true, message: "Введите размеры", trigger: "blur" }
        ]
      },
      {
        key: "weight",
        keyTable: "weight",
        title: "Вес",
        type: "input",
        isArray: false,
        validator: [{ required: true, message: "Введите вес", trigger: "blur" }]
      },
      {
        key: "calories",
        keyTable: "calories",
        title: "Калории",
        type: "input",
        isArray: false,
        validator: [
          { required: true, message: "Введите калории", trigger: "blur" }
        ]
      },
      {
        key: "price",
        keyTable: "price",
        title: "Цена",
        type: "priceInput",
        isArray: false,
        validator: [
          { required: true, message: "Введите цену", trigger: "blur" }
        ]
      }
    ],
    template: {
      table: Table,
      toolbar: ActionToolbar
    },
    dictionaries: {
      preload: ["ProductsList"]
    }
  }),
  Promocodes: new ConfigBlock({
    title: "Промокоды",
    url: "promoCodes",
    middleColumn: [
      {
        key: "description",
        keyTable: "description",
        title: "Описание",
        type: "textarea",
        isArray: false
      },
      {
        key: "code",
        keyTable: "code",
        title: "Промокод",
        type: "input",
        isArray: false,
        validator: [
          { required: true, message: "Введите промокод", trigger: "blur" }
        ]
      },
      {
        key: "parameters",
        keyTable: "parameters",
        title: "Параметры",
        type: "promocode",
        isArray: true,
        validator: [
          { required: true, message: "Введите параметры", trigger: "blur" }
        ],
        defaultValue: {
          order: {
            min_price: null,
            productList: []
          },
          gift: {
            product_id: null,
            discount_order: null
          }
        }
      }
    ],
    template: {
      table: Table,
      toolbar: ActionToolbar
    },
    dictionaries: {
      preload: ["ProductsList"]
    }
  })
};
