import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Dashboard",
    component: () => import("../views/Dashboard.vue"),
  },
  {
    path: "/products/:id",
    props: true,
    component: () => import("../views/base.vue"),
    children: [
      {
        path: "",
        name: "ProductsIndex",
        component: () => import("@/components/views/index.vue"),
      },
      {
        path: "page/:pageId",
        name: "ProductsItem",
        component: () => import("@/components/views/item.vue"),
      },
    ],
  },
  {
    path: "/Products",
    name: "Products",
    component: () => import("@/components/dictionary/products/Item.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
