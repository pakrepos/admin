import axios, { AxiosRequestConfig, Method } from "axios";

// const API_URL = "http://127.0.0.1:8000/api/";
const API_URL = "https://yummepal-backend.herokuapp.com/api/";

function defaultConfig(): AxiosRequestConfig {
  return {
    baseURL: API_URL,
    headers: {
      Authorization: `Bearer ${localStorage.getItem("PAKtokenAPI")}`
    }
  };
}

interface ITypeParam {
  type: string;
}

interface IPageParam {
  from?: number;
  count?: number;
}

interface IDataParam {
  actualDate?: string;
}

interface IGetListDataParam extends IGetItemDataParam {
  name?: string;
}

interface IGetListOrItemDataParam extends ITypeParam, IPageParam, IDataParam {
  id?: number;
}

interface IGetItemDataParam extends ITypeParam, IPageParam {
  id: number;
}

interface IRemoveItemDataParam extends IDataParam {
  data: number[];
}

interface ISaveDataParam extends ITypeParam {
  data: JSON[];
}

export interface ICustomApi {
  part: string;
  method: Method;
}

const createAxios = <T>(axiosConfig: AxiosRequestConfig) =>
  axios.request<T>({ ...axiosConfig, ...defaultConfig() });

const paths = {
  product: {
    GET: "show",
    GETID: "get",
    POST: "create",
    DELETE: "delete",
    PATCH: "update"
  },
  tags: {
    GET: "all",
    GETID: "tag",
    POST: "create",
    DELETE: "delete",
    PATCH: "update"
  },
  ingredients: {
    GET: "all",
    GETID: "ingredient",
    POST: "create",
    DELETE: "delete",
    PATCH: "update"
  },
  parameter: {
    POST: "create",
    DELETE: "delete",
    PATCH: "update"
  },
  promoCodes: {
    GET: "show",
    POST: "create",
    DELETE: "delete",
    PATCH: "update"
  }
};

const api = {
  reference: {
    administration: {
      reference: {
        getList: (params: IGetListDataParam) =>
          createAxios({
            method: "GET",
            url: "/reference/administration/reference",
            params
          }),
        getTreeList: () =>
          createAxios({
            method: "GET",
            url: "/reference/administration/reference/reference-tree"
          }),
        getItem: ({ id, ...params }: IGetItemDataParam) =>
          createAxios({
            method: "GET",
            url: `/reference/administration/reference/${id}`,
            params
          }),
        updateItem: (data: any) =>
          createAxios({
            method: "PUT",
            url: "/reference/administration/reference/",
            data
          }),
        removeItem: (params: IRemoveItemDataParam) =>
          createAxios({
            method: "DELETE",
            url: "/reference/administration/reference/",
            params
          })
      }
    }
  },
  dictionary: (
    type: string,
    isReadOnly: boolean,
    custom: ICustomApi = { part: "default", method: "GET" }
  ) => {
    if (isReadOnly) {
      return {
        getData: ({ id, ...params }: IGetListOrItemDataParam) =>
          createAxios({
            method: "GET",
            url: `/reference/${type}/${id ?? "search"}`,
            params
          }),
        getHistory: ({ id, ...params }: IGetListOrItemDataParam) =>
          createAxios({
            method: "GET",
            url: `/reference/${type}/history/${id}`,
            params
          })
      };
    } else {
      return {
        getToken: ({ data }) =>
          createAxios({
            method: "POST",
            url: `${type}`,
            data
          }),
        getData: ({ id, ...params }: IGetListOrItemDataParam) =>
          createAxios({
            method: "GET",
            url: id
              ? `/${type}/${paths[type]["GETID"]}/${id}`
              : `/${type}/${paths[type]["GET"]}`,
            params
          }),
        addData: ({ data }: ISaveDataParam) =>
          createAxios({
            method: "POST",
            url: `${type}/${paths[type]["POST"]}`,
            data
          }),
        saveData: ({ data, id }) =>
          createAxios({
            method: "PATCH",
            url: `${type}/${paths[type]["PATCH"]}/${id}`,
            data
          }),
        removeData: ({ data }: IRemoveItemDataParam) =>
          createAxios({
            method: "DELETE",
            url: `${type}/${paths[type]["DELETE"]}/${data}`
          }),
        [custom.part]: ({ data, ...params }) =>
          createAxios({
            method: custom.method,
            url: `/reference/${type}/${custom.part}`,
            data,
            params
          })
      };
    }
  }
};

export const createDictionaryApi = (type, isReadonly, custom?: ICustomApi) => ({
  action,
  ...params
}) => {
  const apiDict = api.dictionary(type, isReadonly, custom)[action];

  return apiDict(params);
};

export default api;
