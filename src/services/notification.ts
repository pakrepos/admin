import { notification } from "ant-design-vue";

export interface INoty {
  type?: string;
  message?: string;
  description?: string;
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function useNoty({ type, message, description }: INoty) {
  notification.config({
    duration: 3,
    // closeIcon: " ",
  });

  const openNotification = () => {
    if (type === "success") {
      notification.success({
        message,
        description,
      });
    } else if (type === "error") {
      notification.error({
        message,
        description,
      });
    } else
      notification.open({
        message,
        description,
      });
  };

  return { openNotification };
}
