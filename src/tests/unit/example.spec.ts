import { mount } from "@vue/test-utils";
import Login from "../components/views/Login.vue";

describe("Login.vue", () => {
  test("Check noty message", async () => {
    const wrapper = mount(Login);
    const button = wrapper.find("button");

    await button.trigger("click");

    expect(wrapper.html()).toContain("Вход выполнен успешно");
  });
});
