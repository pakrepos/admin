interface IConfig {
  baseUrlRoute?: string;
  authUrl?: string;
  apiUrl?: string;
}

declare const config: IConfig;
